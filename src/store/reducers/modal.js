import { TOGGLE_MODAL } from '../actions/modal';

const initialState = {
  modalShown: false,
  modalTitle: '',
  modalDescription: '',
  isBasic: true,
};

const toggleModal = (state, payload) => {
  return {
    ...state,
    modalShown: payload.newState,
    modalTitle: payload.title,
    modalDescription: payload.description,
    isBasic: payload.isBasic,
  };
};

export const modal = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      return toggleModal(state, action.payload);

    default:
      return state;
  }
};

export default modal;
