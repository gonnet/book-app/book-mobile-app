import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import React from 'react';
import { Icon as Icons } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from '../shared/constants';

const SettingsRowItem = ({ iconType, iconName, title, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.rowContainer}>
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View style={styles.iconContainer}>
          <Icons
            type={iconType}
            name={iconName}
            color={Colors.COLOR_PRIMARY}
            size={30}
          />
        </View>
        <Text
          style={{
            marginStart: 15,
            color: Colors.textThird,
            fontFamily: 'Poppins-Regular',
            fontSize: 20,
          }}>
          {title}
        </Text>
      </View>
      <Icon
        name="chevron-right"
        color={Colors.COLOR_GREEN_INACTIVE}
        size={21}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    height: 80,
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },

  iconContainer: {
    backgroundColor: 'rgba(0, 151, 167,0.03)',
    padding: 10,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
});

export default SettingsRowItem;
