import { TOGGLE_NOTIFICATION_FLAG } from '../actions/notification';

const initialState = {
  notificationFlag: false
};

const toggleNotificationFlag = (state) => {
  return {
    ...state,
    notificationFlag: !state.notificationFlag
  };
};

export const notification = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_NOTIFICATION_FLAG:
      return toggleNotificationFlag(state);

    default:
      return state;
  }
};

export default notification;
