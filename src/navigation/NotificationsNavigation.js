import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import NotificationsHomeScreen from '../screens/notifications/NotificationsHomeScreen';
import { Colors } from '../shared/constants';
import { TouchableOpacity } from 'react-native';
import MyPostDetailsScreen from '../screens/posts/MyPostDetailsScreen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useTranslation } from 'react-i18next';

const Stack = createStackNavigator();

const NotificationsNavigation = () => {
  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  return (
    <Stack.Navigator initialRouteName="NotificationsHome" screenOptions={{}}>
      <Stack.Screen
        name="NotificationsHome"
        component={NotificationsHomeScreen}
        options={{
          headerTitle: t(`common:notifications_header_notifications_screen`),
          headerStyle: {
            shadowColor: 'black',
            shadowOffset: { width: 2, height: 8 },
            elevation: 5,
            shadowOpacity: 0.26,
          },
          headerTitleStyle: { color: Colors.COLOR_PRIMARY, letterSpacing: 5 },
        }}
      />
      <Stack.Screen
        name="MyPostDetailsScreen"
        component={MyPostDetailsScreen}
        options={({ navigation }) => ({
          headerTransparent: true,
          headerStyle: { height: 70 },
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={30}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
    </Stack.Navigator>
  );
};

export default NotificationsNavigation;
