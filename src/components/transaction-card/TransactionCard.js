import React, { useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, Animated, Easing } from 'react-native';
import { styles } from './styles';
import { Icon as Icons } from 'react-native-elements';
import { Colors } from '../../shared/constants';
import helpers from '../../shared/helpers';

const TransactionCard = ({ transaction, isDisabled, onClick, userName }) => {
  const opacity = useRef(new Animated.Value(0)).current;
  const scale = useRef(new Animated.Value(0.3)).current;

  useEffect(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 300,
      easing: Easing.inOut(Easing.ease),
      useNativeDriver: true,
    }).start();
    Animated.timing(scale, {
      toValue: 1,
      duration: 150,
      easing: Easing.inOut(Easing.ease),
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <Animated.View
      style={[styles.container, { opacity: opacity, transform: [{ scale }] }]}>
      <TouchableOpacity disabled={isDisabled} onPress={onClick}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 4,
          }}>
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={{ color: 'black' }}>
            {transaction.seller_name}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 4,
          }}>
          <Icons
            type="font-awesome-5"
            name="clock"
            size={20}
            color={Colors.COLOR_GREEN_INACTIVE}
            style={{ marginHorizontal: 4 }}
          />
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={{ color: 'black' }}>
            {helpers.parseDate(transaction.created_at)}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 4,
          }}>
          <Icons
            type={transaction.confirmed ? 'font-awesome-5' : 'material-icons'}
            name={transaction.confirmed ? 'check-circle' : 'pending'}
            size={20}
            color={
              transaction.confirmed
                ? Colors.COLOR_GREEN_INACTIVE
                : Colors.COLOR_WARNING
            }
            style={{ marginHorizontal: 4 }}
          />
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={{ color: 'black' }}>
            {transaction.confirmed ? 'Successful' : 'Pending'}
          </Text>
        </View>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default TransactionCard;
