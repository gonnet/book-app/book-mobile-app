import {
  View,
  StyleSheet,
  FlatList,
  RefreshControl,
  Text,
  TouchableOpacity,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import {
  getAllTransactions,
  getSinglePost,
} from '../../infrastructure/api/api';
import { Colors } from '../../shared/constants';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';
import TransactionCard from '../../components/transaction-card/TransactionCard';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

const HistoryScreen = ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectedTransactionsType, setSelectedTransactionsType] =
    useState('sold');
  const [soldTransactions, setSoldTransactions] = useState([]);
  const [boughtTransactions, setBoughtTransactions] = useState([]);
  const user = useSelector(state => state.user.currentUser);

  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const loadTransactions = async (loadSpinner = true) => {
    try {
      loadSpinner ? setLoading(true) : null;
      let response = await getAllTransactions();
      setSoldTransactions(response.data.sold);
      setBoughtTransactions(response.data.bought);
      loadSpinner ? setLoading(false) : null;
    } catch (e) {
      console.log(e);
      loadSpinner ? setLoading(false) : null;
      return;
    }
  };

  const onTransactionPressed = async postId => {
    try {
      setLoading(true);
      let transactionPost = await getSinglePost(postId);
      setLoading(false);
      console.log('this is', transactionPost);
      navigation.navigate('MyPostDetailsScreen', {
        post: transactionPost.data,
      });
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    loadTransactions();
  }, []);

  const onRefresh = async () => {
    setRefreshing(true);
    await loadTransactions(false);
    setRefreshing(false);
  };

  const historyCardRenderer = item => {
    return (
      <TransactionCard
        transaction={item}
        userName={user.name}
        onClick={() => onTransactionPressed(item.post_id)}
      />
    );
  };

  const EmptyPlaceholder = () => {
    return (
      <View style={styles.emptyContainer}>
        <View style={styles.iconWithTextsContainer}>
          <Icon
            name="swap-horiz"
            size={100}
            color={Colors.COLOR_PRIMARY}
            style={{ textShadowOffset: { width: 3, height: 2 } }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>
              {t(`common:no_transaction_title_history_screen`)}
            </Text>
            <Text style={styles.descriptionText}>
              {t(`common:no_transaction_description_history_screen`)}
            </Text>
          </View>
        </View>
        <CustomButtonSecondary
          title={t(`common:refresh_button`)}
          onPress={loadTransactions}
        />
      </View>
    );
  };

  return (
    <>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          height: 70,
          alignItems: 'center',
        }}
      >
        <TouchableOpacity
          onPress={() => {
            setSelectedTransactionsType('sold');
          }}
          style={[
            {
              width: '50%',
              alignItems: 'center',
              height: '100%',
              justifyContent: 'center',
            },
            selectedTransactionsType == 'sold'
              ? {
                  borderBottomWidth: 2,
                  borderColor: Colors.COLOR_PRIMARY_GREEN,
                }
              : null,
          ]}
        >
          <Text
            style={{
              color: Colors.COLOR_PRIMARY_GREEN,
              fontSize: 18,
              fontWeight: '700',
            }}
          >
            {t(`common:sold_label_history_screen`)}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setSelectedTransactionsType('bought');
          }}
          style={[
            {
              width: '50%',
              alignItems: 'center',
              height: '100%',
              justifyContent: 'center',
            },
            selectedTransactionsType == 'bought'
              ? {
                  borderBottomWidth: 2,
                  borderColor: Colors.COLOR_PRIMARY_GREEN,
                }
              : null,
          ]}
        >
          <Text
            style={{
              color: Colors.COLOR_PRIMARY_GREEN,
              fontSize: 18,
              fontWeight: '700',
            }}
          >
            {t(`common:bought_label_history_screen`)}
          </Text>
        </TouchableOpacity>
      </View>
      {selectedTransactionsType == 'sold' && (
        <View style={styles.containerStyle}>
          {soldTransactions.length > 0 ? (
            <FlatList
              style={{ width: '100%' }}
              data={soldTransactions}
              contentContainerStyle={{ paddingBottom: 70 }}
              keyExtractor={item => item.id}
              renderItem={({ item }) => historyCardRenderer(item)}
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
            />
          ) : loading ? null : (
            <EmptyPlaceholder />
          )}
        </View>
      )}

      {selectedTransactionsType == 'bought' && (
        <View style={styles.containerStyle}>
          {boughtTransactions.length > 0 ? (
            <FlatList
              style={{ width: '100%' }}
              data={boughtTransactions}
              contentContainerStyle={{ paddingBottom: 70 }}
              keyExtractor={item => item.id}
              renderItem={({ item }) => historyCardRenderer(item)}
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
            />
          ) : loading ? null : (
            <EmptyPlaceholder />
          )}
        </View>
      )}
      {loading && <LoadingSpinner />}
    </>
  );
};
const styles = StyleSheet.create({
  containerStyle: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.lightBg,
    paddingBottom: 80,
  },

  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.lightBg,
  },

  iconWithTextsContainer: { alignItems: 'center' },

  textContainer: { marginTop: 20, paddingBottom: 10, alignItems: 'center' },

  titleText: {
    fontSize: 22,
    color: Colors.textThird,
    fontWeight: '700',
  },

  descriptionText: { color: Colors.textThird },
});

export default HistoryScreen;
