import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
  I18nManager,
} from 'react-native';
import { getUser, logout } from '../../infrastructure/api/api';
import { AsyncStorageKeys, Colors, ModalTexts } from '../../shared/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import { useSelector } from 'react-redux';
import { toggleLoading } from '../../store/actions/loadingActions';
import Divider from '../../components/divider/Divider';
import { Icon as Icons } from 'react-native-elements';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import LinearGradient from 'react-native-linear-gradient';
import ModalCard from '../../components/modal/Modal';
import { toggleModal } from '../../store/actions/modal';
import SettingsRowItem from '../../components/SettingsRowItem';
import { useTranslation } from 'react-i18next';
import RNRestart from 'react-native-restart';

const SettingsHomeScreen = props => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.loading.loadingState);
  const points = useSelector(state => state.user.points);
  const [modalShown, setModalShown] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const { i18n, t } = useTranslation();
  const setLanguage = lang => {
    i18n.changeLanguage(lang);
    I18nManager.forceRTL(lang == 'ar' ? true : false);
    RNRestart.Restart();
  };

  const onLogoutPressed = async () => {
    setModalShown(false);
    dispatch(toggleLoading(true));
    try {
      await logout();
      await AsyncStorage.removeItem(AsyncStorageKeys.USER_KEY);
      dispatch(toggleLoading(false));
      dispatch(authActions.setCurrentUser(null));
    } catch (e) {
      dispatch(toggleLoading(false));
      dispatch(
        toggleModal(true, ModalTexts.EMPTY_FIELD_TITLE, error?.data?.message),
      );
      setModalShown(true);
    }
  };

  useEffect(() => {
    updatePoints();
  }, []);

  const updatePoints = async () => {
    try {
      const { points } = (await getUser()).data;
      dispatch(authActions.updateUserPoints(points));
    } catch (error) {
      console.log('point', error);
      Alert.alert('Error loading the points', error?.data?.message);
    }
    return;
  };

  const refreshPoints = async () => {
    setRefreshing(true);
    await updatePoints();
    setRefreshing(false);
  };

  const modalLogoutPressed = () => {
    dispatch(
      toggleModal(true, 'Sign out', 'Are you sure you want to logout?', false),
    );
    setModalShown(true);
  };

  const navigateToScreen = screenName => {
    props.navigation.navigate(screenName);
    return;
  };

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={false}
        cancelButton={ModalTexts.MODAL_CANCEL}
        actionButton="Logout"
        actionButtonFunction={onLogoutPressed}
      />
      <ScrollView
        contentContainerStyle={{ flex: 1 }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refreshPoints} />
        }>
        <LinearGradient
          colors={[Colors.COLOR_PRIMARY, Colors.lightBg]}
          style={{ flex: 1, elevation: 0 }}>
          <View style={styles.container}>
            <View
              style={{
                height: '35%',
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 15,
              }}>
              <Image
                source={require('../../assets/confetti.png')}
                style={{ width: '100%', height: '100%' }}
                resizeMode="stretch"
                borderBottomRightRadius={40}
                borderBottomLeftRadius={40}
              />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'absolute',
                  backgroundColor: Colors.COLOR_OFF_WHITE,
                  height: 100,
                  width: 200,
                  borderRadius: 20,
                  opacity: 0.8,
                  shadowColor: 'black',
                  shadowOffset: { width: 0, height: 2 },
                  elevation: 10,
                  shadowOpacity: 0.26,
                }}>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: Colors.COLOR_GREEN_INACTIVE,
                  }}>
                  {t(`common:total_label_settings_screen`)}:
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text
                    style={{
                      fontSize: 28,
                      fontWeight: 'bold',
                      color: Colors.COLOR_PRIMARY_GREEN,
                      marginRight: 6,
                    }}>
                    {points} {t(`common:points_label_settings_screen`)}
                  </Text>
                  <Icons
                    type="font-awesome-5"
                    name="coins"
                    color={Colors.COLOR_PRIMARY}
                    size={26}
                  />
                </View>
              </View>
            </View>

            <ScrollView
              contentContainerStyle={{
                backgroundColor: Colors.lightBg,
                paddingHorizontal: 30,
                borderTopRightRadius: 45,
                borderTopLeftRadius: 45,
                shadowColor: 'black',
                elevation: 45,
                paddingTop: 30,
              }}
              showsVerticalScrollIndicator={false}>
              <SettingsRowItem
                iconType="material-community"
                iconName="account"
                title={t(`common:profile_label_settings_screen`)}
                onPress={() => navigateToScreen('UserProfile')}
              />
              <Divider />
              <SettingsRowItem
                iconType="material-community"
                iconName="qrcode"
                title={t(`common:scan_label_settings_screen`)}
                onPress={() => navigateToScreen('ScanQR')}
              />
              <Divider />
              <SettingsRowItem
                iconType="material"
                iconName="leaderboard"
                title={t(`common:leaderboard_label`)}
                onPress={() => navigateToScreen('Leaderboard')}
              />
              <Divider />
              <SettingsRowItem
                iconType="fontawesome5"
                iconName="history"
                title={t(`common:history_label_settings_screen`)}
                onPress={() => navigateToScreen('History')}
              />
              <Divider />
              <SettingsRowItem
                iconType="fontawesome5"
                iconName="globe"
                title={t(`common:language_button_settings_screen`)}
                onPress={() =>
                  setLanguage(t(`common:language_settings_screen`))
                }
              />
              <Divider />
              <SettingsRowItem
                iconType="feather"
                iconName="log-out"
                title={t(`common:logout_label_settings_screen`)}
                onPress={modalLogoutPressed}
              />
              <Divider />
              <Text
                style={{
                  alignSelf: 'center',
                  marginTop: 20,
                  color: Colors.textThird,
                  fontFamily: 'Poppins-Regular',
                }}
              />

              <Text
                style={{
                  alignSelf: 'center',
                  marginTop: 20,
                  color: Colors.textThird,
                  fontFamily: 'Poppins-Regular',
                }}>
                V1.0
              </Text>
            </ScrollView>
          </View>
        </LinearGradient>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  bodyContainer: {
    backgroundColor: Colors.lightBg,
    flex: 1,
    paddingHorizontal: 30,
    borderTopRightRadius: 45,
    borderTopLeftRadius: 45,
    shadowColor: 'black',
    elevation: 45,
    paddingTop: 30,
  },
});

export default SettingsHomeScreen;
