import { StyleSheet, Text, TextInput, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import Message from '../post/Message';
const InputEdit = props => {
  const [error, setError] = useState(false);
  const {
    name,
    onChange,
    setErrors,
    errorsObject,
    errorMessage,
    ...inputProps
  } = props;
  const checkError = () => {
    let checkNull = props.value == null ? true : !props.value.trim();
    if (checkNull) {
      setError(true);
      setErrors({ ...errorsObject, [name]: true });
      return;
    } else if (props.value.length < 5 && !(name == 'price')) {
      setError(true);
      setErrors({ ...errorsObject, [name]: true });
      return;
    }
    setError(false);
    setErrors({ ...errorsObject, [name]: false });
  };
  return (
    <View style={styles.container}>
      <TextInput
        style={{ ...styles.input, ...props.inputStyle }}
        {...inputProps}
        onChange={onChange}
        onBlur={checkError}
      />
      {error ? (
        <Message messageStyle={styles.message} message={errorMessage} />
      ) : null}
    </View>
  );
};

export default InputEdit;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  input: {
    height: 48,
    paddingStart: 16,
    paddingEnd: 12,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#0000001f',
    borderRadius: 4,
    color: '#000000de',
  },
  message: {
    marginEnd: 8,
  },
});
