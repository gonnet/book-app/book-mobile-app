import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MyPostsScreen from '../screens/posts/MyPostsScreen';
import MyPostDetailsScreen from '../screens/posts/MyPostDetailsScreen';
import AddPostScreen from '../screens/posts/AddPostScreen';
import EditPostScreen from '../screens/posts/EditPostScreen';
import { Colors } from '../shared/constants';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useTranslation } from 'react-i18next';

const Stack = createStackNavigator();

const MyPostsNavigation = () => {
  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  return (
    <Stack.Navigator initialRouteName="MyPosts">
      <Stack.Screen
        name="MyPostsHome"
        component={MyPostsScreen}
        options={() => ({
          headerStyle: {
            shadowColor: 'black',
            shadowOffset: { width: 2, height: 8 },
            elevation: 5,
            shadowOpacity: 0.26,
          },
          headerTitle: t(`common:my_posts_header_my_posts_screen`),
          headerTitleStyle: { color: Colors.COLOR_PRIMARY, letterSpacing: 5 },
        })}
      />
      <Stack.Screen
        name="AddPost"
        component={AddPostScreen}
        options={() => ({
          headerTitle: t(`common:add_post_header`),
        })}
      />
      <Stack.Screen
        name="MyPostDetailsScreen"
        component={MyPostDetailsScreen}
        options={({ navigation }) => ({
          headerStyle: { height: 70 },
          headerTransparent: true,
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={30}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="EditPost"
        component={EditPostScreen}
        options={() => ({
          headerTitle: t(`common:edit_post_header`),
        })}
      />
    </Stack.Navigator>
  );
};

export default MyPostsNavigation;
