import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { Colors } from '../shared/constants';
import { Icon as Icons } from 'react-native-elements';
import helpers from '../shared/helpers';
import { Divider, SpeedDial } from 'react-native-elements';

export default function UserPointCard(props) {
  const getTopThreeColor = () => {
    if (props.rank === 1) {
      return '#FFD700';
    } else if (props.rank === 2) {
      return '#C0C0C0';
    } else if (props.rank === 3) {
      return '#CD7F32';
    }
    return '#80dfff';
  };

  const getTopThreeIcons = (size = 25) => {
    let ranksVector = [1, 2, 3];
    if (ranksVector.includes(props.rank)) {
      return (
        <Icons
          name="medal"
          size={size}
          type="font-awesome-5"
          color={getTopThreeColor()}
        />
      );
    }
    return null;
  };

  if (props.rank === 1) {
    return (
      <View style={{ alignItems: 'center' }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#FFD700',
            backgroundColor: 'rgba(255, 215, 0, .5)',
            width: 60,
            height: 60,
            borderRadius: 30,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 5,
          }}
        >
          <Text
            style={styles.userIconText}
            numberOfLines={1}
            adjustsFontSizeToFit
          >
            {props.name !== undefined
              ? helpers.getUserNameNickName(props.name)
              : null}
          </Text>
          <View style={{ position: 'absolute', left: 20, right: -36, top: -1 }}>
            <Icons
              size={16}
              name="crown"
              type="font-awesome-5"
              color={'#FFD700'}
            />
          </View>
        </View>
        <View
          style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}
        >
          <Text
            style={{
              color: '#e6e600',
              padding: 10,
              fontStyle: 'italic',
              fontSize: 21,
              fontWeight: 'bold',
            }}
          >
            {props.name}
          </Text>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View
        style={[styles.containerRank, { backgroundColor: getTopThreeColor() }]}
      >
        <Text style={styles.rank}>{props.rank} </Text>
      </View>

      <Divider orientation="vertical" width={0.6} style={{ height: 10 }} />

      <View
        style={[
          styles.card,
          {
            backgroundColor:
              props.rank % 2 != 0 ? 'rgba(242, 242, 242, .48)' : 'white',
            borderTopRightRadius: 20,
          },
        ]}
      >
        <View style={{ flexDirection: 'row' }}>
          <View
            style={{
              backgroundColor: '#A9A9A9',
              width: 30,
              height: 30,
              borderRadius: 20,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 5,
            }}
          >
            <Text
              style={styles.userIconText}
              numberOfLines={1}
              adjustsFontSizeToFit
            >
              {props.name !== undefined
                ? helpers.getUserNameNickName(props.name)
                : null}
            </Text>
          </View>

          <View style={{ flexDirection: 'column', paddingStart: 6 }}>
            <Text style={{ fontWeight: 'bold', color: '#808080' }}>
              {props.name}
            </Text>
            <Text style={{ fontSize: 12, color: '#C0C0C0' }}>
              {props.points} Points
            </Text>
          </View>
        </View>

        {getTopThreeIcons()}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
    display: 'flex',
  },
  containerRank: {
    borderRadius: 40 / 2,
    flex: 1,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginEnd: 4,
  },
  rank: {
    color: 'white',
    fontWeight: 'bold',
    textAlignVertical: 'center',
  },
  card: {
    flex: 6,
    paddingHorizontal: 10,
    height: 70,
    marginVertical: 6,
    borderRadius: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  userIconText: {
    color: Colors.lightBg,
    fontSize: 26,
    fontFamily: 'Poppins-Bold',
    marginHorizontal: 2,
  },
});
