import React from 'react';
import ProductsNavigation from './UserProductsNavigation';
import MyPostsNavigation from './MyPostsNavigation';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SettingsNavigation from './SettingsNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import { Colors } from '../shared/constants';
import NotificationsNavigation from './NotificationsNavigation';
import AddPostScreen from '../screens/posts/AddPostScreen';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withSequence,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { toggleNotification } from '../store/actions/notification';

const Tab = createBottomTabNavigator();

const AppNavigation = () => {
  const notificationFlag = useSelector(state => state.notification.notificationFlag);
  const addRotationProgress = useSharedValue(0);
  const dispatch = useDispatch()
  const rotateAddIcon = useAnimatedStyle(() => {
    return {
      transform: [{ rotate: withSpring(`${addRotationProgress.value}deg`) }],
    };
  }, [addRotationProgress]);

  const notificationsProgress = useSharedValue(0);
  const rotateNotificationIcon = useAnimatedStyle(() => {
    return {
      transform: [{ rotate: withSpring(`${notificationsProgress.value}deg`) }],
    };
  }, [notificationsProgress]);

  const myPostsProgress = useSharedValue(0);
  const rotateMyPosts = useAnimatedStyle(() => {
    return {
      transform: [{ rotateX: withSpring(`${myPostsProgress.value}deg`) }],
    };
  }, [myPostsProgress]);

  const homeProgress = useSharedValue(1);
  const animateHome = useAnimatedStyle(() => {
    return {
      transform: [{ scale: withSpring(homeProgress.value) }],
    };
  }, [homeProgress]);

  const settingsProgress = useSharedValue(0);
  const animateSettings = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: withSpring(settingsProgress.value) }],
    };
  }, [settingsProgress]);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarShowLabel: false,
        headerShown: false,
        tabBarStyle: {
          position: 'absolute',
          backgroundColor: Colors.lightBg,
          left: 10,
          right: 10,
          bottom: 10,
          borderRadius: 20,
          shadowOffset: { width: 0, height: 2 },
          borderTopWidth: 0,
          height: 55,
          elevation: 5,
          shadowColor: 'black',
        },
        tabBarActiveTintColor: Colors.COLOR_PRIMARY,
        tabBarInactiveTintColor: Colors.tabbarInactiveTint,
      })}
    >
      <Tab.Screen
        name="Home"
        component={ProductsNavigation}
        listeners={{
          tabPress: () => {
            homeProgress.value = withSequence(
              withTiming(2, { duration: 100 }),
              withTiming(1, { duration: 100 }),
            );
          },
        }}
        options={{
          title: 'Home',
          tabBarIcon: ({ color, focused }) => {
            return (
              <Animated.View
                style={[
                  { shadowColor: Colors.COLOR_PRIMARY, elevation: 30 },
                  animateHome,
                ]}
              >
                <Icon name="home" color={color} size={24} />
              </Animated.View>
            );
          },
        }}
      />

      <Tab.Screen
        name="Notifications"
        component={NotificationsNavigation}
        listeners={{
          tabPress: () => {
            notificationsProgress.value = withSequence(
              withTiming(-50, { duration: 80 }),
              withRepeat(withTiming(50, { duration: 200 }), 2, true),
              withTiming(0, { duration: 80 }),
            );
            notificationFlag ? dispatch(toggleNotification()) : null
          },
        }}
        options={{
          title: 'Notifications',
          tabBarIcon: ({ color, focused }) => {
            return (
              <Animated.View
                style={[
                  { shadowColor: Colors.COLOR_PRIMARY, elevation: 30 },
                  rotateNotificationIcon,
                ]}
              >
                <Icon name="bell" color={color} size={24} />
                {notificationFlag && <View style = {{height: 10, width: 10, borderRadius:5, backgroundColor: 'red', position: 'absolute', right: -5, top: -5}}/>}
              </Animated.View>
            );
          },
        }}
      />
      <Tab.Screen
        name="AddPost"
        component={AddPostScreen}
        listeners={{
          tabPress: () => {
            addRotationProgress.value = withSequence(
              withTiming(100, { duration: 80 }),
              withTiming(0, { duration: 80 }),
            );
          },
        }}
        options={({ navigation }) => ({
          title: 'New Post',
          tabBarIcon: ({ color, focused }) => {
            return (
              <Animated.View
                style={[
                  {
                    position: 'absolute',
                    bottom: 10,
                    height: 65,
                    width: 65,
                    borderRadius: 32.5,
                    backgroundColor:
                      color == Colors.COLOR_PRIMARY
                        ? Colors.COLOR_PRIMARY
                        : Colors.COLOR_GREEN_INACTIVE,
                    alignItems: 'center',
                    justifyContent: 'center',
                    shadowColor:
                      color == Colors.COLOR_PRIMARY
                        ? Colors.COLOR_PRIMARY
                        : Colors.colorPrimaryInActive,
                    elevation: 35,
                  },
                  rotateAddIcon,
                ]}
              >
                <Feather name="plus" color={Colors.whiteText} size={50} />
              </Animated.View>
            );
          },
        })}
      />
      <Tab.Screen
        name="MyPosts"
        component={MyPostsNavigation}
        listeners={{
          tabPress: () => {
            myPostsProgress.value = withSequence(
              withTiming(360, { duration: 100 }),
              withTiming(0, { duration: 100 }),
            );
          },
        }}
        options={{
          title: 'My Posts',
          tabBarIcon: ({ color, focused }) => {
            return (
              <Animated.View
                style={[
                  { shadowColor: Colors.COLOR_PRIMARY, elevation: 30 },
                  rotateMyPosts,
                ]}
              >
                <Icon name="heart" color={color} size={24} />
              </Animated.View>
            );
          },
        }}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsNavigation}
        listeners={{
          tabPress: () => {
            settingsProgress.value = withSequence(
              withTiming(10, { duration: 100 }),
              withTiming(-10, { duration: 100 }),
              withTiming(0, { duration: 100 }),
            );
          },
        }}
        options={{
          title: 'Settings',
          tabBarIcon: ({ color, focused }) => {
            return (
              <Animated.View
                style={[
                  { shadowColor: Colors.COLOR_PRIMARY, elevation: 30 },
                  animateSettings,
                ]}
              >
                <Icon name="bars" color={color} size={24} />
              </Animated.View>
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigation;
