import { createStore, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';
import rootReducer from './reducers/index';
import Config from 'react-native-config';

const middleware = [thunk];
// if (Config.ENV !== 'prod') {

// }

const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
