import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Overlay } from 'react-native-elements';

const Modal = props => {
  return (
    <Overlay
      overlayStyle={styles.modalContainer}
      isVisible={props.visible}
      onBackdropPress={props.onBackdropPress}
    >
      <Text style={styles.modalTitle}>{props.title}</Text>
      <Text style={styles.modalDescription}>{props.body}</Text>
      <View style={styles.modalDeleteButtons}>{props.children}</View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    width: '80%',
    borderRadius: 8,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    shadowOpacity: 0.26,
  },

  modalDeleteButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 10,
  },

  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  modalDescription: {
    textAlign: 'center',
    margin: 10,
  },
});

export default Modal;
