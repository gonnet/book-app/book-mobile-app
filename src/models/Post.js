class Post {
  constructor(
    id,
    userId,
    title,
    imageURL,
    description,
    price,
    numberOfPapers,
    status,
    contactMethod,
    exchangeDetails,
    postTypeId,
    createdAt,
    updatedAt,
    user,
    postTypeReadable,
  ) {
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.imageURL = imageURL;
    this.price = price;
    this.description = description;
    this.numberOfPapers = numberOfPapers;
    this.status = status;
    this.contactMethod = contactMethod;
    this.exchangeDetails = exchangeDetails;
    this.postTypeId = postTypeId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.user = user;
    this.postTypeReadable = postTypeReadable;
  }
}

export default Post;
