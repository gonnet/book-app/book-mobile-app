import {
  View,
  StyleSheet,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import React, { useEffect, useState, useRef } from 'react';
import CustomInput from '../../components/post/CustomInput';
import CustomDropdown from '../../components/post/CustomDropdown';
import { useDispatch, useSelector } from 'react-redux';
import { Colors, ModalTexts } from '../../shared/constants';
import { ImagePicker } from '../../components/ImagePicker';
import {
  addPost,
  uploadImage,
  getPostTypes,
} from '../../infrastructure/api/api';
import PostDto from '../../infrastructure/api/dto/Post';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import Label from '../../components/post/Label';
import { toggleReaload } from '../../store/actions/loadingActions';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';
import { toggleModal } from '../../store/actions/modal';
import ModalCard from '../../components/modal/Modal';
import _ from 'lodash';
import { useTranslation } from 'react-i18next';

const AddPostScreen = ({ navigation }) => {
  const user = useSelector(state => state.user.currentUser);
  const [loading, setLoading] = useState(false);
  const [modalShown, setModalShown] = useState(false);
  const [flagImage, setFlagImage] = useState(true);
  const reload = useSelector(state => state.loading.reload);
  const dispatch = useDispatch();
  const didMountRef = useRef(false);

  const [title, setTitle] = useState('');
  const [image, setImage] = useState(null);
  const [description, setDescription] = useState('');
  const [pickupLocation, setPickupLocation] = useState('');
  const [postType, setPostType] = useState(1);
  const [postTypes, setPostTypes] = useState([]);
  const [price, setPrice] = useState(null);
  const [exchangeDetails, setExchangeDetails] = useState(null);

  const [errors, setErrors] = useState({
    title: false,
    description: false,
    pickupLocation: false,
    price: false,
    exchangeDetails: false,
    image: false,
  });

  const { i18n, t } = useTranslation();

  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const [selectedContactMethod, setSelectedContactMethod] = useState(
    t(`common:contact_method_whatsApp_label`),
  );

  const dataContactMethods = [
    { label: t(`common:contact_method_whatsApp_label`), value: 'WHATSAPP' },
    { label: t(`common:contact_method_phone_label`), value: 'PHONE' },
    { label: t(`common:contact_method_both_label`), value: 'BOTH' },
  ];

  const minimumLengthError = t(`common:minimum_length_field_add_post`);

  useEffect(() => {
    const fetchDataPostTypes = async () => {
      let res = await getPostTypes();
      let dataPostType = [];
      let dataPostTypeLabels = [
        t(`common:post_type_sell_label`),
        t(`common:post_type_exchange_label`),
        t(`common:post_type_donate_label`),
      ];

      dataPostType = res.data.map(type => ({
        label: type.name,
        value: type.id,
      }));

      for (let i = 0; i < dataPostType.length; i++) {
        dataPostType[i].label = dataPostTypeLabels[i];
      }
      setPostTypes(dataPostType);
    };

    fetchDataPostTypes();
  }, []);

  const upload = async image => {
    try {
      const formData = prepareFormData(image);
      return await uploadImage(formData);
    } catch (e) {
      console.error(e);
    }
  };

  const prepareFormData = image => {
    const data = new FormData();
    data.append('image', {
      ...image,
      name: image.fileName,
      uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
    });
    return data;
  };

  const checkInputs = (input, inputName) => {
    let textInput = input ? input.trim() : null;

    if (!textInput) {
      setErrors({
        ...errors,
        [inputName]: t(`common:field_is_required`),
      });
    } else if (!(textInput.length >= 5) && !(inputName == 'price')) {
      setErrors({ ...errors, [inputName]: minimumLengthError });
    } else {
      setErrors({ ...errors, [inputName]: '' });
    }
  };

  const imageRequiredWithExtension = () => {
    let hasAllowExtension = checkImageExtension();
    if (!image && flagImage) {
      setErrors({ ...errors, image: t(`common:field_is_required`) });
      return;
    } else if (!hasAllowExtension) {
      setErrors({
        ...errors,
        image: t(`common:image_support`),
      });
      return;
    }
    setErrors({ ...errors, image: '' });
    return;
  };

  const checkImageExtension = () => {
    if (!image && flagImage) return false;

    if (flagImage) {
      let extension = image['uri'].substr(image['uri'].lastIndexOf('.') + 1);
      let extensionAllowed = ['jpeg', 'png', 'jpg', 'gif', 'svg'];
      if (extensionAllowed.includes(extension)) return true;

      return false;
    }
    return true;
  };

  const validate = obj => {
    imageRequiredWithExtension();
    for (let key in obj) {
      if (obj[key].length > 0 || obj[key] === false) return false;
    }

    return true;
  };

  const onChangePriceExchange = text => {
    let selectedPostType = postTypes.find(type => type.value == postType).label;

    if (selectedPostType === t(`common:post_type_sell_label`)) {
      setPrice(text);
      setExchangeDetails(null);
      setErrors({ ...errors, exchangeDetails: '' });
      return;
    } else if (selectedPostType === t(`common:post_type_exchange_label`)) {
      setExchangeDetails(text);
      setPrice(null);
      setErrors({ ...errors, price: '' });
      return;
    }
  };

  const checkCommaPrice = () => {
    return !price ? null : price.replace(',', '');
  };

  const checkDonate = async () => {
    let selectedPostType = postTypes.find(type => type.value == postType).label;

    if (selectedPostType === t(`common:post_type_donate_label`)) {
      setExchangeDetails(null);
      setPrice(null);
      errors.price = '';
      errors.exchangeDetails = '';
      return;
    }
    return;
  };

  const submitHandler = async () => {
    setLoading(true);
    setFlagImage(true);
    await checkDonate();
    await checkFillInputs();

    let valid = validate(errors);
    if (!valid) {
      setLoading(false);
      showErrorsModal();
      return;
    }

    let validateImage = checkImageExtension();
    if (!validateImage) {
      setLoading(false);
      return;
    }

    try {
      let uploadedImageRes = await upload(image);
      let { image_url } = uploadedImageRes.data;

      let priceWithNoComma = await checkCommaPrice();

      const postDto = new PostDto(
        user.id,
        title,
        image_url,
        description,
        pickupLocation,
        postType,
        priceWithNoComma,
        exchangeDetails,
        selectedContactMethod,
        'INREVIEW',
      );

      let res = await addPost(postDto);
      setLoading(false);

      if (res.status == 201) {
        navigation.navigate('MyPosts');
        dispatch(toggleReaload(!reload));
        await resetInputs();
        resetErrors();
        return;
      }
    } catch (error) {
      setLoading(false);
      console.log('here modal', error);
      setModalShown(true);
      dispatch(
        toggleModal(true, ModalTexts.EMPTY_FIELD_TITLE, error.data.message),
      );
    }

    return;
  };

  const resetInputs = async () => {
    setTitle('');
    setImage(null);
    setDescription('');
    setPickupLocation('');
    setExchangeDetails(null);
    setPrice(null);
  };

  const resetErrors = () => {
    const resetErrorsObject = {
      title: false,
      description: false,
      pickupLocation: false,
      price: false,
      exchangeDetails: false,
      image: false,
    };

    Object.assign(errors, resetErrorsObject);
    setFlagImage(false);
  };

  const checkFillInputs = async () => {
    let listOfValues = {
      title: title,
      description: description,
      pickupLocation: pickupLocation,
    };
    let selectedPostType = postTypes.find(type => type.value == postType).label;

    if (selectedPostType === t(`common:post_type_sell_label`)) {
      listOfValues['price'] = price;
    } else if (selectedPostType === t(`common:post_type_exchange_label`)) {
      listOfValues['exchangeDetails'] = exchangeDetails;
    }

    _.forEach(listOfValues, function (value, key) {
      if (!value) {
        errors[key] = t(`common:field_is_required`);
      } else {
        errors[key] = '';
      }
    });
  };

  useEffect(() => {
    if (didMountRef.current) {
      return imageRequiredWithExtension();
    }

    didMountRef.current = true;
  }, [image, flagImage]);

  const showErrorsModal = () => {
    setModalShown(true);
    dispatch(
      toggleModal(
        true,
        ModalTexts.USER_FAILED_ACTION,
        ModalTexts.FORM_ERROR,
        true,
      ),
    );
  };

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />

      <KeyboardAvoidingView behavior={'height'} style={{ flex: 1 }}>
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          keyboardShouldPersistTaps={'handled'}
        >
          <View style={styles.container}>
            <Label
              iconName="format-title"
              iconType="material-community"
              label={t('common:title_label')}
              requiredStar
            />
            <CustomInput
              placeholder={t('common:title_placeholder')}
              autoCorrect={true}
              maxLength={250}
              value={title}
              onChangeText={setTitle}
              error={errors.title}
              requiredStar={true}
              onEndEditing={() => {
                checkInputs(title, 'title');
              }}
            />
            <Label
              iconName="comment-dots"
              iconType="font-awesome-5"
              label={t('common:description_label')}
              requiredStar
            />
            <CustomInput
              placeholder={t('common:description_placeholder')}
              multiline={true}
              numberOfLines={5}
              maxLength={800}
              value={description}
              onChangeText={setDescription}
              requiredStar={true}
              inputStyle={styles.inputMultiStyle}
              error={errors.description}
              onEndEditing={() => {
                checkInputs(description, 'description');
              }}
            />

            <Label
              iconName="format-list-bulleted-type"
              iconType="material-community"
              label={t('common:post_type_label')}
              requiredStar
            />
            <CustomDropdown
              data={postTypes}
              selectedValue={postType}
              onValueChange={(itemValue, itemIndex) => setPostType(itemValue)}
            />
            {postTypes.find(type => type.value === postType)?.label ===
            t(`common:post_type_sell_label`) ? (
              <>
                <Label
                  labelContainerStyle={{ marginTop: 0 }}
                  iconName="currency-gbp"
                  iconType="material-community"
                  label={t('common:price_label')}
                  requiredStar
                />
                <CustomInput
                  placeholder={t('common:price_placeholder')}
                  keyboardType="decimal-pad"
                  maxLength={10}
                  value={price}
                  inputContainerStyle={styles.inputContainerPriceExchange}
                  inputStyle={styles.inputPriceExchange}
                  onChangeText={onChangePriceExchange}
                  error={errors.price}
                  onEndEditing={() => {
                    checkInputs(price, 'price');
                  }}
                />
              </>
            ) : null}
            {postTypes.find(type => type.value === postType)?.label ===
            t('common:post_type_exchange_label') ? (
              <>
                <Label
                  labelContainerStyle={{ marginTop: 0 }}
                  iconName="exchange-alt"
                  iconType="font-awesome-5"
                  label={t('common:exchange_details_label')}
                  requiredStar
                />
                <CustomInput
                  placeholder={t('common:exchange_details_placeholder')}
                  multiline={true}
                  numberOfLines={5}
                  maxLength={800}
                  value={exchangeDetails}
                  inputContainerStyle={styles.inputContainerPriceExchange}
                  inputStyle={{
                    marginVertical: 5,
                    borderColor: '#4c5265',
                    height: 68,
                    textAlignVertical: 'top',
                  }}
                  onChangeText={onChangePriceExchange}
                  error={errors.exchangeDetails}
                  onEndEditing={() => {
                    checkInputs(exchangeDetails, 'exchangeDetails');
                  }}
                />
              </>
            ) : null}

            <ImagePicker
              setImage={setImage.bind(this)}
              imageData={image}
              error={errors.image}
              errorMessage={errors.image}
            />

            <Label
              iconName="card-account-phone-outline"
              iconType="material-community"
              label={t('common:contact_method_label')}
              requiredStar
            />
            <CustomDropdown
              data={dataContactMethods}
              selectedValue={selectedContactMethod}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedContactMethod(itemValue)
              }
            />

            <Label
              iconName="map-marker-alt"
              iconType="font-awesome-5"
              label={t('common:location_label')}
              requiredStar
            />
            <CustomInput
              placeholder={t('common:location_placeholder')}
              multiline={true}
              numberOfLines={5}
              maxLength={800}
              inputStyle={styles.inputMultiStyle}
              value={pickupLocation}
              onChangeText={setPickupLocation}
              error={errors.pickupLocation}
              onEndEditing={() => {
                checkInputs(pickupLocation, 'pickupLocation');
              }}
              requiredStar={true}
            />

            <View style={{ marginVertical: 25, marginBottom: 70 }}>
              <CustomButtonSecondary
                title={t('common:post_button')}
                onPress={submitHandler}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightBg,
  },
  inputContainerPriceExchange: {
    marginTop: 0,
    flex: 1,
    marginEnd: 16,
  },
  inputPriceExchange: {
    marginVertical: 5,
    borderColor: '#4c5265',
  },
  inputMultiStyle: {
    height: 98,
    textAlignVertical: 'top',
  },
  saveButton: {
    backgroundColor: Colors.COLOR_PRIMARY,
    borderRadius: 5,
  },
  saveButtonContainer: {
    marginHorizontal: 50,
    height: 50,
    width: 200,
    marginVertical: 10,
  },
});
export default AddPostScreen;
