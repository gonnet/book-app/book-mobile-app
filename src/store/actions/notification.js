export const TOGGLE_NOTIFICATION_FLAG =  'TOGGLE_NOTIFICATION_FLAG';

const toggleNotification = () => {
  return {
    type: TOGGLE_NOTIFICATION_FLAG,
  };
};

export { toggleNotification };
